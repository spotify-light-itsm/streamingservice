package sst.spotify.streaming_service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.io.IOException;

@RestController
@RequestMapping("/audio")
public class StreamingRestController {

    @Value("${audio-directory}")
    private String AUDIO_DIRECTORY;
    private final StreamingRestService streamingRestService;

    public StreamingRestController(StreamingRestService streamingRestService) {
        this.streamingRestService = streamingRestService;
    }

    @GetMapping("/{fileName}")
    public Mono<ResponseEntity<byte[]>> streamAudio(@RequestHeader(value = "Range", required = false) String httpRangeList,
                                                    @PathVariable("fileName") String fileName) throws IOException {
        return Mono.just(streamingRestService.getContent(AUDIO_DIRECTORY,
                fileName + ".mp3",
                httpRangeList));
    }
}