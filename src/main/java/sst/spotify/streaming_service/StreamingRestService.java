package sst.spotify.streaming_service;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

@Service
public class StreamingRestService {
    private static final int BYTE_RANGE = 128;

    public ResponseEntity<byte[]> getContent(String directory, String fileName, String range) throws IOException {
        final String FILETYPE = "mp3";
        Long fileSize = Optional.ofNullable(fileName)
                .map(file -> Paths.get(getFilePath(directory), file))
                .map(this::sizeFromFile)
                .orElse(0L);
        if (range == null) {
            return getFullSong(FILETYPE, fileSize, directory, fileName);
        }
        return getPartialDataFromSong(range, FILETYPE, fileSize, directory, fileName);
    }


    // Method is called when no range is passed in the parameters of the "getContent" method
    private ResponseEntity getFullSong(String filetype, long fileSize, String directory, String fileName)
    {
        try {
            // Method adds appropriate HTTP status code to the response
                return ResponseEntity.status(HttpStatus.OK)
                        .header("Content-Type", "audio/" + filetype)
                        .header("Content-Length", String.valueOf(fileSize))
                        // full song is returned in body
                        .body(readByteRange(directory, fileName, 0, fileSize - 1));
            } catch (IOException e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }


    // Method is called when a range is passed in the parameters of the "getContent" method.
    private ResponseEntity getPartialDataFromSong(String range, String filetype, long fileSize, String directory, String fileName) throws IOException {
        long rangeStart = 0;
        String[] ranges = range.split("-");
        rangeStart = Long.parseLong(ranges[0].substring(6));
        long rangeEnd = ranges.length > 1 ? Long.parseLong(ranges[1]) : fileSize - 1;
        if (fileSize < rangeEnd) rangeEnd = fileSize - 1;
        String contentLength = String.valueOf((rangeEnd - rangeStart) + 1);
        return ResponseEntity.status(HttpStatus.PARTIAL_CONTENT)
                .header("Content-Type", "audio/" + filetype)
                .header("Accept-Ranges", "bytes")
                .header("Content-Length", contentLength)
                .header("Content-Range", "bytes" + " " + rangeStart + "-" + rangeEnd + "/" + fileSize)
                // only partial song is returned
                .body(readByteRange(directory, fileName, rangeStart, rangeEnd));
    }




    public byte[] readByteRange(String location, String filename, long start, long end) throws IOException {
        Path path = Paths.get(getFilePath(location), filename);
        try (InputStream inputStream = (Files.newInputStream(path));
             ByteArrayOutputStream bufferedOutputStream = new ByteArrayOutputStream()) {
            byte[] data = new byte[BYTE_RANGE];
            int nRead;
            while ((nRead = inputStream.read(data, 0, data.length)) != -1) {
                bufferedOutputStream.write(data, 0, nRead);
            }
            bufferedOutputStream.flush();
            byte[] result = new byte[(int) (end - start) + 1];
            System.arraycopy(bufferedOutputStream.toByteArray(), (int) start, result, 0, result.length);
            return result;
        }
    }




    private String getFilePath(String location) {
        return new File(location).getAbsolutePath();
    }




    private Long sizeFromFile(Path path) {
        try {
            return Files.size(path);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return 0L;
    }
}
