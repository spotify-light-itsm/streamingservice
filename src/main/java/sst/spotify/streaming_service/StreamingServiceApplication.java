package sst.spotify.streaming_service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableEurekaClient
@SpringBootApplication
public class StreamingServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(StreamingServiceApplication.class, args);
    }

}
