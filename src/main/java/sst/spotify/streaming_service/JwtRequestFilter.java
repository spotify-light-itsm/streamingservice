package sst.spotify.streaming_service;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

@Component
public class JwtRequestFilter extends OncePerRequestFilter {

    @Value("${auth-checker-url-user}")
    String authCheckUrlString;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String authHeader = request.getHeader(HttpHeaders.AUTHORIZATION);

        if (authHeader != null && authHeader.startsWith("Bearer ")) {
            // URL authCheckUrl = new URL("http://user-mgmt-api:8081/auth/test/role/user");
            URL authCheckUrl = new URL(authCheckUrlString);
            HttpURLConnection con = (HttpURLConnection) authCheckUrl.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Authorization", authHeader);
            if(con.getResponseCode() == HttpURLConnection.HTTP_OK){
                UsernamePasswordAuthenticationToken authentication =
                        new UsernamePasswordAuthenticationToken("username", null, AuthorityUtils.commaSeparatedStringToAuthorityList(""));
                SecurityContextHolder.getContext().setAuthentication(authentication);
            }
        }
        filterChain.doFilter(request, response);
    }
}
