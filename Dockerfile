#################################################################
# Stage 1: BUILD & TEST                                         #
#################################################################
FROM gradle:6.7.0-jdk8 AS build
COPY --chown=gradle:gradle . /home/gradle/src
WORKDIR /home/gradle/src
RUN gradle build --no-daemon
#################################################################
# Stage 2: RUNTIME                                              #
#################################################################
FROM openjdk:8-jre-alpine
EXPOSE 8089
RUN addgroup -S spring && adduser -S spring -G spring
USER spring:spring
COPY --from=build /home/gradle/src/build/libs/*.jar app.jar
ENTRYPOINT ["java", "-jar", "app.jar"]