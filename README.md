**DOKU für Streaming Service**
Von Michael Lehenauer und Malina Klein

Erledigte Schritte 

1.) Java Projekt erstellt mit Spring Boot und Gradle 

2.) Dockerfile erstellt

3.) Dockerimage erstellt (docker build -t streamingservice:latest .) und Java Projekt 
im Container laufen lassen (docker run -v audiovolume:/app/static/audio -p 8089:8089 --network usermanagementservice-master_default streamingservice:latest)

4.) StreamingRestController und StreamingRestService erstellt. 
Es kann über die URL: http://127.0.0.1:8089/audio/{id} (mp3 Endung muss nicht mit angegeben werden) eine Audiodatei abgespielt werden. 
Über BYTE_RANGE kann die Position des Songs mitgegeben werden.


5.) .gitlab-ci.yml hinzugefügt 

API-Beschreibung unter streamingAPI-Doku.yml 

Tokenvalidierung über Aufruf der User-Management REST-API. 

Komponentendiagramm:

![Komponentendiagramm](Komponentendiagramm.JPG)

Sequenzdiagramm:

![Sequenzdiagramm](Sequenzdiagramm.JPG)



